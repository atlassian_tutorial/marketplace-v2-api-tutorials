#!/bin/bash
if [[ -n "$1" ]]; then
  java -cp `find target -name '*-with-dependencies.jar'` "$@"
else
  echo "usage: $0 CLASS_NAME [PARAMETERS]"
  echo
  echo "CLASS_NAME may be:"
  echo "  CreateAddon"
  echo "  CreateAddonVersion"
  echo "  CreateVendor"
  echo "  GetAddon"
  echo "  GetAddonPricing"
  echo "  ListAddons"
  echo "  ListVendors"
  echo
  echo "For help on parameters, type $0 CLASS_NAME -?"
fi
