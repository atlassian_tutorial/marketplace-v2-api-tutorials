import com.atlassian.fugue.Option;
import com.atlassian.marketplace.client.MarketplaceClient;
import com.atlassian.marketplace.client.MpacException;
import com.atlassian.marketplace.client.api.AddonQuery;
import com.atlassian.marketplace.client.model.Addon;

import com.beust.jcommander.Parameter;

/**
 * Example of querying add-on details, with or without an add-on version.
 * <p>
 * You do not need to be authenticated, unless you are querying a private add-on.
 */
public class GetAddon extends BaseExample {

    private static class Params extends BaseParams {
        @Parameter(names={"-a", "--addon"}, required=true, description="The add-on key")
        String addonKey;

        @Parameter(names={"-v", "--with-version"}, description="Show latest version for add-on")
        boolean withVersion;
    }

    public static void main(String[] args) throws MpacException {
        Params params = readParams(args, new Params());
        MarketplaceClient client = createClient(params);

        AddonQuery query = AddonQuery.builder().withVersion(params.withVersion).build();

        Option<Addon> result = client.addons().getByKey(params.addonKey, query);
        for (Addon a: result)
        {
            System.out.println(client.toJson(a));
        }
        if (!result.isDefined())
        {
            System.err.println("not found");
        }
    }
}
