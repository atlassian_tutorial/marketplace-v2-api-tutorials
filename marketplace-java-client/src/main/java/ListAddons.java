import java.util.List;

import com.atlassian.fugue.Option;
import com.atlassian.marketplace.client.MarketplaceClient;
import com.atlassian.marketplace.client.MpacException;
import com.atlassian.marketplace.client.api.AddonQuery;
import com.atlassian.marketplace.client.api.ApplicationKey;
import com.atlassian.marketplace.client.api.Cost;
import com.atlassian.marketplace.client.api.HostingType;
import com.atlassian.marketplace.client.api.Page;
import com.atlassian.marketplace.client.api.PageReference;
import com.atlassian.marketplace.client.api.QueryBounds;
import com.atlassian.marketplace.client.model.AddonSummary;
import com.atlassian.marketplace.client.model.AddonVersionSummary;

import com.beust.jcommander.Parameter;
import com.google.common.base.Function;

import static com.atlassian.fugue.Option.option;
import static com.atlassian.fugue.Option.some;

/**
 * Example of querying a list of add-ons, with optional parameters for constraining the query.
 * <p>
 * You do not need to be authenticated.
 */
public class ListAddons extends BaseExample {

    public static class Params extends BaseParams {
        @Parameter(names={"-a", "--application"}, description="Show only add-ons compatible with this application")
        String appKey;

        @Parameter(names={"-b", "--build"}, description="Show only add-ons compatible with this application build number")
        int appBuild;

        @Parameter(names="--categories", variableArity = true, description="Show only add-ons with the specified categories")
        List<String> categories;

        @Parameter(names={"-c", "--cost"}, description="Show only add-ons with the specified paid/free status")
        Cost cost;

        @Parameter(names={"-h", "--hosting-type"}, description="Show only add-ons with the specified hosting model")
        HostingType hostingType;

        @Parameter(names={"-q", "--query"}, description="Show only add-ons that match the search text")
        String searchText;

        @Parameter(names={"-v", "--with-version"}, description="Show latest version for each add-on")
        boolean withVersion;

        @Parameter(names={"--view"}, description="Show add-ons filtered or sorted using the specified parameter")
        AddonQuery.View view;
    }

    private static int showAddons(Iterable<AddonSummary> addons, int index) {
        for (AddonSummary a: addons) {
            // may or may not have an embedded version
            String latestVersion = a.getVersion().flatMap(new Function<AddonVersionSummary, Option<String>>() {
                    public Option<String> apply(AddonVersionSummary v) {
                        return v.getName();
                    }
                }).getOrElse("");
            System.out.println(String.format("%04d  %s  %s -- %s", index, a.getName(), latestVersion, a.getKey()));
            index++;
        }
        return index;
    }

    public static void main(String[] args) throws MpacException {
        Params params = readParams(args, new Params());
        MarketplaceClient client = createClient(params);

        AddonQuery.Builder queryBuilder = AddonQuery.builder()
            .bounds(QueryBounds.limit(some(50)))
            .withVersion(params.withVersion)
            .cost(option(params.cost))
            .hosting(option(params.hostingType))
            .searchText(option(params.searchText))
            .view(option(params.view));

        if (params.appKey != null) {
            queryBuilder.application(some(ApplicationKey.valueOf(params.appKey)));
            if (params.appBuild > 0) {
                queryBuilder.appBuildNumber(some(params.appBuild));
            }
        }

        if (params.categories != null) {
            queryBuilder.categoryNames(params.categories);
        }

        AddonQuery query = queryBuilder.build();

        try {
            Page<AddonSummary> addons = client.addons().find(query);
            System.out.println(String.format("Total add-ons found: %d", addons.totalSize()));
            int n = showAddons(addons, 1);
            while (addons.getNext().isDefined()) {
                for (PageReference<AddonSummary> next: addons.getNext()) {
                    addons = client.getMore(next);
                    n = showAddons(addons, n);
                }
            }
        } finally {
            client.close();
        }
    }
}
