import com.atlassian.marketplace.client.MarketplaceClient;
import com.atlassian.marketplace.client.MpacException;
import com.atlassian.marketplace.client.api.Page;
import com.atlassian.marketplace.client.api.PageReference;
import com.atlassian.marketplace.client.api.VendorQuery;
import com.atlassian.marketplace.client.model.VendorSummary;

import com.beust.jcommander.Parameter;

/**
 * Example of querying a list of vendors.
 * <p>
 * You do not need to be authenticated, unless you use the "-m"/"--mine" option to query
 * the vendors associated with the current user.
 */
public class ListVendors extends BaseExample {

    public static class Params extends BaseParams {
        @Parameter(names={"-m", "--mine"},
            description="Lists only the vendors for this user instead of all vendors")
        boolean mine = false;
    }

    private static int showVendors(Iterable<VendorSummary> vendors, int index) {
        for (VendorSummary v: vendors) {
            System.out.println(String.format("%04d  %s", index, v.getName()));
            index++;
        }
        return index;
    }

    public static void main(String[] args) throws MpacException {
        Params params = readParams(args, new Params());
        MarketplaceClient client = createClient(params);
        
        VendorQuery query = VendorQuery.builder().forThisUserOnly(params.mine).build();

        Page<VendorSummary> vendors = client.vendors().find(query);
        System.out.println(String.format("Total vendors found: %d", vendors.totalSize()));
        int n = showVendors(vendors, 1);
        while (vendors.getNext().isDefined()) {
            for (PageReference<VendorSummary> next: vendors.getNext()) {
                vendors = client.getMore(next);
                n = showVendors(vendors, n);
            }
        }
    }
}
