import java.io.File;

import com.atlassian.fugue.Option;
import com.atlassian.marketplace.client.MarketplaceClient;
import com.atlassian.marketplace.client.MpacException;
import com.atlassian.marketplace.client.api.ImageId;
import com.atlassian.marketplace.client.api.ImagePurpose;
import com.atlassian.marketplace.client.api.Page;
import com.atlassian.marketplace.client.api.VendorId;
import com.atlassian.marketplace.client.api.VendorQuery;
import com.atlassian.marketplace.client.model.Addon;
import com.atlassian.marketplace.client.model.AddonStatus;
import com.atlassian.marketplace.client.model.AddonVersion;
import com.atlassian.marketplace.client.model.AddonVersionStatus;
import com.atlassian.marketplace.client.model.ModelBuilders;
import com.atlassian.marketplace.client.model.VendorSummary;

import com.beust.jcommander.Parameter;

import static com.atlassian.fugue.Option.none;
import static com.atlassian.fugue.Option.option;
import static com.atlassian.fugue.Option.some;

/**
 * Example of creating a new add-on listing.
 * <p>
 * This requires you to be authenticated (see {@link BaseExample#getOptionalAuthenticationCredentials}).
 * <p>
 * Note that if you create a public add-on, it will not be visible to others until it has
 * been approved by Atlassian.
 */
public class CreateAddon extends BaseExample {

    // We need to include all of the parameters used by CreateAddonVersion, because creating an add-on
    // requires us to provide its first version at the same time.
    public static class Params extends CreateAddonVersion.Params {
        @Parameter(names={"-n", "--name"}, required=true, description="The add-on name")
        String addonName;
        
        @Parameter(names={"-u", "--summary"}, description="The add-on summary")
        String addonSummary;

        @Parameter(names={"-o", "--logo"}, description="Image file for the add-on logo")
        File logoFile;
    }
    
    private static Addon buildAddon(Params params, MarketplaceClient client) throws MpacException {
        AddonVersion version = CreateAddonVersion.buildVersion(params, client);
        
        // Use the same parameter to specify both the add-on version status and the add-on status
        // (that is, if you're creating a public add-on, its first version must be public, and if
        // you're creating a private add-on, its first version must be private)
        AddonStatus status = (params.status == AddonVersionStatus.PRIVATE) ? AddonStatus.PRIVATE : AddonStatus.PUBLIC;
        
        // Get the list of vendors associated with the current user.  If there's more than one,
        // for this example we'll just use the first one.
        Page<VendorSummary> vendors = client.vendors().find(VendorQuery.builder().forThisUserOnly(true).build());
        VendorId vendorId;
        if (vendors.size() >= 1) {
            vendorId = vendors.iterator().next().getId();
        } else {
            throw new MpacException("The current user is not associated with any vendors");
        }
        
        Option<ImageId> logoId = none();
        if (params.logoFile != null) {
            System.err.println(String.format("Uploading %s...", params.logoFile.getPath()));
            logoId = some(client.assets().uploadImage(params.logoFile, ImagePurpose.LOGO));
        }

        return ModelBuilders.addon()
                .key(params.addonKey)
                .name(params.addonName)
                .logo(logoId)
                .status(status)
                .summary(option(params.addonSummary))
                .vendor(vendorId)
                .version(some(version))
                .build();
    }
    
    public static void main(String[] args) throws MpacException {
        Params params = readParams(args, new Params());
        MarketplaceClient client = createClient(params);

        Addon addon = buildAddon(params, client);
        
        System.err.println("Creating addon...");
        Addon result = client.addons().createAddon(addon);

        System.err.println(String.format("Created addon: %s", result.getSelfUri()));
        System.out.println(client.toJson(result));
    }
}
