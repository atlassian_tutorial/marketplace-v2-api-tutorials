import java.net.URI;

import com.atlassian.fugue.Option;
import com.atlassian.marketplace.client.MarketplaceClient;
import com.atlassian.marketplace.client.MarketplaceClientFactory;
import com.atlassian.marketplace.client.http.HttpConfiguration;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.Parameter;

import org.slf4j.LoggerFactory;

import static com.atlassian.fugue.Option.some;

/**
 * Base class for all the Marketplace API Java client examples.  Provides some standard
 * parameters that can be used with any of the examples, and the logic for setting up a
 * MarketplaceClient in either an authenticated or unauthenticated state.
 */
public class BaseExample {
    
    public static class BaseParams {
        @Parameter(names="--server",
            description="Use this URL instead of the default Marketplace server")
        public URI serverUri;
        
        @Parameter(names={"-u", "--user"},
            description="Authenticate with this user account")
        public String username;
        
        @Parameter(names={"-w", "--password"},
            description="Authenticate with this password (non-interactive)")
        public String password;

        @Parameter(names={"-p", "--password-prompt"}, password=true,
            description="Enter password for authentication")
        public String passwordInteractive;

        @Parameter(names="--show-http",
            description="Show content of all HTTP requests and responses")
        public boolean showHttp;
        
        @Parameter(names="--show-urls",
            description="Show URLs of all Marketplace requests")
        public boolean showUrls;
        
        @Parameter(names={"-?", "--help", "--usage"}, help=true,
            description="Show supported options")
        public boolean usage;
    }

    protected static <T extends BaseParams> T readParams(String[] args, T params) {
        JCommander parser = new JCommander(params);
        try {
            parser.parse(args);
        } catch (Exception e) {
            System.err.println(e.getMessage());
            parser.usage();
            System.exit(1);
        }

        if (params.usage) {
            parser.usage();
            System.exit(0);
        }
        
        if (params.showHttp) {
            enableExtraLogging("org.apache.http.wire", true);
        }
        if (params.showUrls) {
            enableExtraLogging("com.atlassian.marketplace.client", false);
        }
        
        return params;
    }

    protected static void enableExtraLogging(String packageName, boolean debug) {
        // Note that we are assuming the slf4j implementation is Logback.  This is contrary to
        // the normal abstract usage of slf4j, but allows us to reconfigure log levels on the fly.
        ch.qos.logback.classic.Logger logger =
            (ch.qos.logback.classic.Logger) LoggerFactory.getLogger(packageName);
        logger.setLevel(debug ? ch.qos.logback.classic.Level.DEBUG : ch.qos.logback.classic.Level.INFO);
    }
    
    protected static <T extends BaseParams> Option<HttpConfiguration.Credentials> getOptionalAuthenticationCredentials(T params) {
        if (params.username == null) {
            // "-u"/"--user" parameter was not specified; don't use any credentials. 
            return Option.none();
        }
        
        // If "-u" or "--user" was specified, then we also need either "-p"/"-password-prompt" (which
        // gets a password from the console; JCommander takes care of this) or "-w"/"--password"
        // (which specifies the password directly on the command line).
        if (params.password == null && params.passwordInteractive == null) {
            System.err.println("Must specify password with -p or -w if username is specified");
            System.exit(0);
        }
        
        String password = (params.password == null) ? params.passwordInteractive : params.password;
        return some(new HttpConfiguration.Credentials(params.username, password));
    }
    
    protected static <T extends BaseParams> MarketplaceClient createClient(T params) {
        URI serverUri = (params.serverUri == null) ? MarketplaceClientFactory.DEFAULT_MARKETPLACE_URI :
            params.serverUri;
        
        for (HttpConfiguration.Credentials credentials: getOptionalAuthenticationCredentials(params)) {
            // User/password credentials were provided
            System.err.println(String.format("Using server %s, authenticated as \"%s\"...",
                    serverUri, params.username));
            HttpConfiguration config = HttpConfiguration.builder()
                    .credentials(some(credentials))
                    .build();
            return MarketplaceClientFactory.createMarketplaceClient(serverUri, config);
        }
        
        // No credentials were provided
        System.err.println(String.format("Using server %s without authentication...", serverUri));
        return MarketplaceClientFactory.createMarketplaceClient(serverUri);
    }
}
