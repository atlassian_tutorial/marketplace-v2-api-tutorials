import java.io.File;

import com.atlassian.fugue.Option;
import com.atlassian.marketplace.client.MarketplaceClient;
import com.atlassian.marketplace.client.MpacException;
import com.atlassian.marketplace.client.api.ArtifactId;
import com.atlassian.marketplace.client.model.AddonVersion;
import com.atlassian.marketplace.client.model.AddonVersionStatus;
import com.atlassian.marketplace.client.model.LicenseType;
import com.atlassian.marketplace.client.model.ModelBuilders;
import com.atlassian.marketplace.client.model.PaymentModel;

import com.beust.jcommander.Parameter;

import org.joda.time.DateTime;
import org.joda.time.LocalDate;

import static com.atlassian.fugue.Option.none;
import static com.atlassian.fugue.Option.option;
import static com.atlassian.fugue.Option.some;

/**
 * Example of creating a new version for an existing add-on.
 * <p>
 * This requires you to be authenticated (see {@link BaseExample#getOptionalAuthenticationCredentials}).
 */
public class CreateAddonVersion extends BaseExample {

    public static class Params extends BaseParams {
        @Parameter(names={"-a", "--addon"}, required=true, description="The add-on key")
        String addonKey;
        
        @Parameter(names={"-v", "--version"}, required=true, description="The version string")
        String versionName;
        
        @Parameter(names={"-b", "--build"}, required=true, description="The build number")
        int versionBuildNumber;
        
        @Parameter(names={"-l", "--license"}, required=true, description="The license type key, such as 'gpl'")
        String licenseType;
        
        @Parameter(names={"-y", "--payment-model"}, description="The payment model")
        PaymentModel paymentModel = PaymentModel.FREE;
        
        @Parameter(names={"-s", "--status"}, description="Specify public/private status")
        AddonVersionStatus status = AddonVersionStatus.PRIVATE;
        
        @Parameter(names={"-r", "--release-summary"}, description="The release summary")
        String releaseSummary;
        
        @Parameter(names={"-f", "--file"}, description="Upload this file as an add-on artifact")
        File file;
    }
    
    public static AddonVersion buildVersion(Params params, MarketplaceClient client) throws MpacException {
        LocalDate releaseDate = new DateTime().toLocalDate();
        
        Option<LicenseType> licenseType = none();
        if (params.licenseType != null) {
            licenseType = client.licenseTypes().getByKey(params.licenseType);
            if (!licenseType.isDefined()) {
                throw new MpacException(String.format("Unknown license type: %s", params.licenseType));
            }
        }

        Option<ArtifactId> artifactId = none();
        if (params.file != null) {
            System.err.println(String.format("Uploading %s...", params.file.getPath()));
            artifactId = some(client.assets().uploadAddonArtifact(params.file));
        }

        return ModelBuilders.addonVersion()
                .artifact(artifactId)
                .name(params.versionName)
                .buildNumber(params.versionBuildNumber)
                .licenseType(licenseType)
                .paymentModel(params.paymentModel)
                .status(params.status)
                .releaseDate(releaseDate)
                .releaseSummary(option(params.releaseSummary))
                .build();
    }
    
    public static void main(String[] args) throws MpacException {
        Params params = readParams(args, new Params());
        MarketplaceClient client = createClient(params);

        AddonVersion version = buildVersion(params, client);
        
        System.err.println("Creating version...");
        AddonVersion result = client.addons().createVersion(params.addonKey, version);

        System.err.println(String.format("Created version: %s", result.getSelfUri()));
        System.out.println(client.toJson(result));
    }
}
