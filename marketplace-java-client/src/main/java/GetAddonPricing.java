import com.atlassian.marketplace.client.MarketplaceClient;
import com.atlassian.marketplace.client.MpacException;
import com.atlassian.marketplace.client.api.PricingType;
import com.atlassian.marketplace.client.model.AddonPricing;
import com.atlassian.marketplace.client.model.AddonPricingItem;

import com.beust.jcommander.Parameter;

/**
 * Example of querying add-on pricing.  Attempts to show Server and/or Cloud pricing, whichever
 * is available.
 * <p>
 * You do not need to be authenticated.
 */
public class GetAddonPricing extends BaseExample {

    public static class Params extends BaseParams {
        @Parameter(names={"-a", "--addon"}, required=true, description="The add-on key")
        String addonKey;
    }

    private static void queryPricing(MarketplaceClient client, String addonKey, PricingType type) throws MpacException {
        System.err.println(String.format("Querying %s pricing for %s...", type, addonKey));
        for (AddonPricing p: client.addons().getPricing(addonKey, type)) {
            System.out.println(String.format("%s has %s pricing as follows:", addonKey, type));
            for (AddonPricingItem pi: p.getItems()) {
                System.out.println(String.format("   %.2f  %s (%s)", pi.getAmount(), pi.getEditionDescription(),
                    (pi.getMonthsValid() == 1) ? "monthly" : "annual"));
            }
            return;
        }
        System.out.println(String.format("%s has no %s pricing", addonKey, type));
    }

    public static void main(String[] args) throws MpacException {
        Params params = readParams(args, new Params());
        MarketplaceClient client = createClient(params);

        queryPricing(client, params.addonKey, PricingType.SERVER);
        queryPricing(client, params.addonKey, PricingType.CLOUD);
    }
}
