import java.io.File;

import com.atlassian.fugue.Option;
import com.atlassian.marketplace.client.MarketplaceClient;
import com.atlassian.marketplace.client.MpacException;
import com.atlassian.marketplace.client.api.ImageId;
import com.atlassian.marketplace.client.api.ImagePurpose;
import com.atlassian.marketplace.client.model.ModelBuilders;
import com.atlassian.marketplace.client.model.Vendor;

import com.beust.jcommander.Parameter;

import static com.atlassian.fugue.Option.none;
import static com.atlassian.fugue.Option.some;

/**
 * Example of creating a vendor listing.
 * <p>
 * This requires you to be authenticated (see {@link BaseExample#getOptionalAuthenticationCredentials}).
 * Your Atlassian account will be listed as the first user for the new vendor.
 */
public class CreateVendor extends BaseExample {

    public static class Params extends BaseParams {
        @Parameter(names={"-n", "--name"}, required=true, description="The vendor name")
        String name;
        
        @Parameter(names={"-e", "--email"}, required=true, description="The vendor email")
        String email;

        @Parameter(names={"-o", "--logo"}, description="Image file for the vendor logo")
        File logoFile;
    }

    public static void main(String[] args) throws MpacException {
        Params params = readParams(args, new Params());
        MarketplaceClient client = createClient(params);
        
        Option<ImageId> logoId = none();
        if (params.logoFile != null) {
            System.err.println(String.format("Uploading %s...", params.logoFile.getPath()));
            logoId = some(client.assets().uploadImage(params.logoFile, ImagePurpose.LOGO));
        }

        Vendor vendor = ModelBuilders.vendor()
            .name(params.name)
            .email(params.email)
            .logo(logoId)
            .build();

        System.err.println("Creating vendor...");
        Vendor result = client.vendors().createVendor(vendor);

        System.err.println(String.format("Created vendor: %s", result.getSelfUri()));
        System.out.println(client.toJson(result));
    }
}
