<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>
    <parent>
        <groupId>com.atlassian.pom</groupId>
        <artifactId>public-pom</artifactId>
        <version>3.0.98</version>
    </parent>
    <groupId>com.atlassian.marketplace</groupId>
    <artifactId>marketplace-java-client-examples</artifactId>
    <version>1.0-SNAPSHOT</version>
    <name>Atlassian Marketplace API Java Client Examples</name>
    <packaging>jar</packaging>

    <scm>
        <connection>scm:git:git@bitbucket.org:atlassian_tutorial/marketplace-tutorial.git</connection>
        <developerConnection>scm:git:git@bitbucket.org:atlassian_tutorial/marketplace-tutorial.git</developerConnection>
        <url>https://bitbucket.org/atlassian_tutorial/marketplace-tutorial</url>
        <tag>HEAD</tag>
    </scm>

    <dependencies>
        <!--
            The marketplace-client-java dependencies jar includes the client library itself
            plus all of its third-party requirements.  If you need to use a different version
            of any of those, you can import them yourself and remove the <classifier> property
            here so that you're only importing the client library jar.
        -->
        <dependency>
            <groupId>com.atlassian.marketplace</groupId>
            <artifactId>marketplace-client-java</artifactId>
            <version>${marketplace-client.version}</version>
            <classifier>dependencies</classifier>
            <scope>compile</scope>
        </dependency>

        <dependency>
            <groupId>org.slf4j</groupId>
            <artifactId>slf4j-api</artifactId>
            <version>${slf4j.version}</version>
            <scope>compile</scope>
        </dependency>

        <!--
            Helper code for command-line parsing
        -->
        <dependency>
            <groupId>com.beust</groupId>
            <artifactId>jcommander</artifactId>
            <version>${jcommander.version}</version>
        </dependency>
        
        <!--
            Logback, the standard implementation of slf4j; you may substitute any slf4j logging
            implementation 
        -->
        <dependency>
            <groupId>ch.qos.logback</groupId>
            <artifactId>logback-classic</artifactId>
            <version>${logback.version}</version>
        </dependency>

        <dependency>
            <groupId>ch.qos.logback</groupId>
            <artifactId>logback-core</artifactId>
            <version>${logback.version}</version>
        </dependency>

        <!--
            Note, jcl-over-slf4j is only required if you want to be able to include debug-level
            output from org.apache.http in your log.
        -->
        <dependency>
            <groupId>org.slf4j</groupId>
            <artifactId>jcl-over-slf4j</artifactId>
            <version>${slf4j.version}</version>
            <scope>compile</scope>
        </dependency>
    </dependencies>

    <build>
        <plugins>
            <plugin>
                <artifactId>maven-compiler-plugin</artifactId>
                <configuration>
                    <source>${jdkLevel}</source>
                    <target>${jdkLevel}</target>
                </configuration>
            </plugin>
            <plugin>
                <artifactId>maven-assembly-plugin</artifactId>
                <executions>
                    <execution>
                        <phase>package</phase>
                        <goals>
                            <goal>single</goal>
                        </goals>
                    </execution>
                </executions>
                <configuration>
                    <descriptorRefs>
                        <descriptorRef>jar-with-dependencies</descriptorRef>
                    </descriptorRefs>
                </configuration>
            </plugin>
        </plugins>
    </build>
    
    <properties>
        <jdkLevel>1.7</jdkLevel>
        <source.encoding>UTF-8</source.encoding>
        <project.build.sourceEncoding>${source.encoding}</project.build.sourceEncoding>
        <project.reporting.outputEncoding>${source.encoding}</project.reporting.outputEncoding>

        <jcommander.version>1.48</jcommander.version>
        <logback.version>1.1.7</logback.version>
        <marketplace-client.version>2.0.7</marketplace-client.version>
        <slf4j.version>1.7.18</slf4j.version>
    </properties>
</project>
