# Marketplace API Tutorials

This repository contains examples of how to use the Atlassian Marketplace REST API to perform tasks such as finding available add-ons and publishing new add-on versions.

Further information can be found [here on developer.atlassian.com](https://developer.atlassian.com/market/developing-for-the-marketplace/marketplace-api).

## JSON examples

The files in `json-examples` are for testing common Marketplace API operations from a Unix shell, or any REST client of your choice.

All `.json` files are examples, which you should edit to add the specific properties of whatever add-on or other entity you want to create. Any property marked "((like this))" *must* be edited.

The Linux shell scripts in `scripts` provide shortcuts for some common operations, using the `curl` command to perform HTTP requests. For the scripts that accept an optional `QUERY_STRING` parameter, if you provide this parameter it should be just as it would appear in a URL, for instance `application=jira&withVersion=true`; you may need to enclose the whole string in quotes to prevent the punctuation from confusing the shell.

If you are an Atlassian employee using a test instance of the Marketplace server rather than the production server, set the environment variable `MARKETPLACE_URL` to the base URL of the test server, for instance:

    export MARKETPLACE_URL=https://marketplace.stg.internal.atlassian.com

## Java examples

The code in `marketplace-java-client` demonstrates how to use the Marketplace API Java Client for some common tasks.

To run one of these examples:

* build the project: `mvn clean package`
* run: `./run-example.sh $CLASS_NAME $PARAMETERS`
* see available parameters: `./run-example.sh $CLASS_NAME -?`

Parameters that can be used with any of the examples:

* `-u USERNAME` or `--user USERNAME`: Adds authentication using the specified Atlassian account. Must be combined with one of the following password parameters:
* `-w PASSWORD` or `--password PASSWORD`: Specifies the password for your Atlassian account. Since this will cause the password to be visible in the terminal and your shell history, it's preferable to instead use `-p`.
* `-p` or `--password-interactive`: Prompts you to enter the password separately.
* `--show-http`: Shows the content of all HTTP requests and responses to and from Marketplace as they occur. Note that responses may not be readable due to gzip compression.
* `--show-urls`: Shows just the URLs of HTTP requests to Marketplace. Note that if the same URL appears several times, it will usually only produce a single request, due to HTTP caching.
