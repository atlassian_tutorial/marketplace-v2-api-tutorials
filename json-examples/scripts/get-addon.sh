#!/bin/bash

# Queries the properties of an existing add-on, displaying them as a JSON document.
#
# Examples of valid query parameters you may add:
# "withVersion=true"  include information about the latest available add-on version
# "withVersion=true&application=jira&applicationBuild=70107"
#                     include information about the latest version that is compatible with JIRA 7.0

if [[ -z "$1" ]]; then
    echo usage: $0 addon_key [query_string]
    exit 1
fi

ADDON_KEY=$1
shift
QUERY_PARAMS=$@

RESOURCE=${MARKETPLACE_URL:-https://marketplace.atlassian.com}/rest/2.0-beta/addons/$ADDON_KEY

if [[ -n "$QUERY_PARAMS" ]]; then
    curl -v $RESOURCE?$QUERY_PARAMS
else
    curl -v $RESOURCE
fi
