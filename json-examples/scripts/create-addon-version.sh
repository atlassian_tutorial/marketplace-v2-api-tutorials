#!/bin/bash

# Submits a new version of an existing add-on to Marketplace.  You must provide the add-on version
# properties in a JSON file; see json/create-addon-version-minimal.json for an example of the minimal
# required properties, or json/create-add-on-version-full.json for all of the usable properties.

if [[ -z "$3" ]]; then
    echo usage: $0 username addon_key json_file_path
    exit 1
fi

USERNAME=$1
ADDON_KEY=$2
FILE_PATH=$3

RESOURCE=${MARKETPLACE_URL:-https://marketplace.atlassian.com}/rest/2.0-beta/addons/$ADDON_KEY/versions

curl -v --basic --user $USERNAME -H "Content-Type: application/json" --data-binary @$FILE_PATH $RESOURCE
