#!/bin/bash

# Uploads an add-on file to Marketplace, to be used in an add-on version that you are planning
# to create.
#
# If successful, it will parse the JSON response and display just the resource URL of the
# file you uploaded, which you should copy into the _links.artifact.href property of your new
# add-on version.

if [[ -z "$2" ]]; then
    echo usage: $0 username addon_file_path
    exit 1
fi

USERNAME=$1
FILE_PATH=$2
LOGICAL_FILE_NAME=`basename $FILE_PATH`

RESOURCE="${MARKETPLACE_URL:-https://marketplace.atlassian.com}/rest/2.0-beta/assets/artifact?file=$LOGICAL_FILE_NAME"

curl -v --basic --user $USERNAME -H "Content-Type: application/octet-stream" --data-binary @$FILE_PATH $RESOURCE | \
    sed -n -e 's/.*"self":{"href":"\([^"]*\)".*/\1/p'
