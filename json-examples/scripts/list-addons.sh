#!/bin/bash

# Queries Marketplace for a list of add-ons, displaying the result as a JSON document.
#
# Examples of valid query parameters you may add to constrain the query:
# "application=jira"             show only add-ons compatible with JIRA
# "application=jira&cost=free"   show only free JIRA add-ons
# "withVersion=true"             include information about the latest available add-on version
# "offset=10"                    skip the first 10 results

QUERY_PARAMS=$@

RESOURCE=${MARKETPLACE_URL:-https://marketplace.atlassian.com}/rest/2.0-beta/addons

if [[ -n "$QUERY_PARAMS" ]]; then
    RESOURCE="$RESOURCE?$QUERY_PARAMS"
fi

curl -v $RESOURCE | \
    # the following sed command just reformats the JSON response to make it a little more readable \
    sed -e "s/},{\"_links\"/},\\
  {\"_links\"/g" -e "s/\"addons\":\[/\"addons\":[\\
  /g" -e "s/\]},\"count/\\
]},\"count/g"
