#!/bin/bash

# Queries the available versions of an application, displaying the result as a JSON document.
# The usual purpose of this is to discover the build numbers for each version.

if [[ -z "$1" ]]; then
    echo usage: $0 application_key
    exit 1
fi

APP_KEY=$1

RESOURCE=${MARKETPLACE_URL:-https://marketplace.atlassian.com}/rest/2.0-beta/applications/$APP_KEY/versions

curl -v $RESOURCE | \
    # the following sed command just reformats the JSON response to make it a little more readable \
    sed -e "s/},{/},\\
  {/g" -e "s/\"versions\":\[/\"versions\":[\\
  /g" -e "s/\]}/\\
]}/g"
