#!/bin/bash

# Uploads an image file to Marketplace, to be used in some other entity that you are going to
# create-- for instance, the logo for an add-on, or a screenshot image for an add-on version.
#
# If successful, it will parse the JSON response and display just the resource URL of the
# content you uploaded, which you should copy into the appropriate link property of your new
# entity (for instance, into _links.logo.href if it is an add-on logo).

if [[ -z "$3" ]]; then
    echo "usage: $0 username image_type image_file_path"
    echo "image_type may be 'logo', 'banner', 'screenshot', 'thumbnail', or 'hero'"
    exit 1
fi

USERNAME=$1
IMAGE_TYPE=$2
FILE_PATH=$3
LOGICAL_FILE_NAME=`basename $FILE_PATH`

RESOURCE="${MARKETPLACE_URL:-https://marketplace.atlassian.com}/rest/2.0-beta/assets/image/$IMAGE_TYPE?file=$LOGICAL_FILE_NAME"

curl -v --basic --user $USERNAME -H "Content-Type: application/octet-stream" --data-binary @$FILE_PATH $RESOURCE | \
    sed -n -e 's/.*"self":{"href":"\([^"]*\)".*/\1/p'
